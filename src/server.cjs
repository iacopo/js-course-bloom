const http = require('http')

http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' })
    res.end('Hello World! 🤣')
}).listen(8081, () => console.log('Visita la pagina http://localhost:8081/'))
