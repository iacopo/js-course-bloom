## Windows

1 - Installare https://git-scm.com/downloads
scegliendo "Use Git From Git Bash only" come opzione quando richiesto

2 - Installare https://nodejs.org/it/download/

3 - Installare https://code.visualstudio.com/  
Installa estensione https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint  
Installa estensione https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

4 - Crea una cartella "Projects" nel desktop (oppure dove preferisci)

5 - Apri il programma "Git Bash"  
Questo programma è una `command line interface` [se vuoi saperne di più](https://it.wikipedia.org/wiki/Interfaccia_a_riga_di_comando)

6 - Incolla questo comando `cd /c/Users/NOME-UTENTE/Desktop/Projects` nel prompt e dai invio  
Sostituisci NOME-UTENTE con il tuo nome utente  
Se precedentemente non avevi creato la cartella nel `Desktop` sostituisci con il path corretto

7 - Incolla questo comando `git -c http.sslVerify=false clone https://bitbucket.org/iacopo/js-course-bloom.git` nel prompt e dai invio

8 - Incolla questo comando `cd js-course-bloom` nel prompt e dai invio

9 - Incolla questo comando `npm install` nel prompt e dai invio

10 - Avviare il programma con il comando `npm run console-hello`  
Se vedi "Hello World!" è tutto ok!

11 - Avviare il server con il comando `npm run server-hello`  
Se visiti il sito `http://localhost:8081/` dovresti vedere "Hello World!"  
`ctrl+c` (per terminare il processo)

## MacOS

1 - Aprire il programma `Terminal`
Questo programma è una `command line interface` [se vuoi saperne di più](https://it.wikipedia.org/wiki/Interfaccia_a_riga_di_comando)

2 - Inserisci il comando `git --version` e premi invio
se non è installato procedi con installazione https://git-scm.com/download/mac scegliendo opzione `Homebrew`

3 - Installare https://nodejs.org/it/download/

4 - Installare https://code.visualstudio.com/  
Installa estensione https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint  
Installa estensione https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

5 - Torna al programma `Terminal`

6 - Dai il comando `cd ~`

7 - Dai il comando `mkdir Projects`

8 - Dai il comando `cd Projects`

9 - Dai il comando `git -c http.sslVerify=false clone https://bitbucket.org/iacopo/js-course-bloom.git`

10 - Dai il comando `cd js-course-bloom`

11 - Dai il comando `npm install`

12 - Avviare il programma con il comando `npm run console-hello`  
Se vedi "Hello World!" è tutto ok!

13 - Avviare il server con il comando `npm run server-hello`  
Se visiti il sito `http://localhost:8081/` dovresti vedere "Hello World!"  
`ctrl+c` (per terminare il processo)
